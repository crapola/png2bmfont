import struct

# References:
# https://www.angelcode.com/products/bmfont/doc/file_format.html

def font(texture_file_name:str)->bytes:
	CHAR_W=8
	CHAR_H=8
	#
	z=b'\0'
	B=lambda x:struct.pack('B',x)
	S=lambda x:struct.pack('h',x)
	I=lambda x:struct.pack('I',x)
	#

	header=b''.join((b'BMF',B(3),))

	info=(
	S(8), # fontSize
	B(1<<4), # bit 4=fixedHeight
	B(0), # charSet
	S(100), # stretchH 100% means no stretch
	B(1), # aa Supersampling 1=none
	z,z,z,z, # padding
	B(2),B(1), # spacing
	z, # outline
	b"Arial\0", #fontName
	)

	common=(
	S(8), # lineHeight
	S(8), # base
	S(128), # scaleW Width of texture
	S(128), # scaleH Height of texture
	S(1), # pages Number of texture pages.
	B(0), # packed
	B(0),B(0),B(0),B(4)
	)

	page=(
	bytes(texture_file_name,'ansi')+b'\0',
	)


	x=8
	y=8
	char_id=48
	char=(
	I(char_id),
	S(x),S(y),
	S(CHAR_W),S(CHAR_H),
	S(0),S(0),
	S(CHAR_W),
	B(0), # page
	B(4), # chnl
	)


	def block(number,b)->bytes:
		b=b''.join(b)
		b=B(number)+I(len(b))+b
		return b

	x=header+block(1,info)+block(2,common)+block(3,page)+block(4,char)
	return x

def main():
	b=font("amsdos.png")
	print(b)
	#exit()
	with open("D:/Programmation/Godot/Tests/test.fnt","wb") as file:
		file.write(b)

if __name__=="__main__":
	main()