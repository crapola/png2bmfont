def font(name:str,texture_file_name:str)->str:
	info={
	'face':name,
	'size':8,
	'bold':0,
	'italic':0,
	'charset':'""',
	'unicode':0,
	'stretchH':100,
	'smooth':1,
	'aa':1,
	'padding':"0,0,0,0",
	'spacing':"0,0"
	}
	common={
	'lineHeight':8,
	'base':0,
	'scaleW':128,
	'scaleH':128,
	'pages':1,
	'packed':0
	}
	page={
	'id':0,
	'file':texture_file_name
	}
	chars={
	'count':256-32,
	}
	clist=[]
	for i in range(0,256-32):
		clist.append({
		'id':i+32,
		'x':8*(i%16),
		'y':(i//16)*8,
		'width':8,
		'height':8,
		'xoffset':0,
		'yoffset':0,
		'xadvance':8,
		'page':0,
		'chnl':0,
		'letter':ascii(chr(i+32)),
		})
	def stringify(d):
		sd=" ".join([str(k)+"="+str(v) for k,v in d.items()])
		return sd
	x="info "+stringify(info)+"\n"
	x+="common "+stringify(common)+"\n"
	x+="page "+stringify(page)+"\n"
	x+="chars "+stringify(chars)+"\n"
	for d in clist:
		x+="char "+stringify(d)+"\n"
	return x

def main():
	source="amsdos.png"
	name=source.split('.')[0]
	target=name+'.fnt'
	name=name.capitalize()
	#target="D:/Programmation/Godot/Tests/amsdos.fnt"
	with open(target,"w") as file:
		file.write(font(name,source))

if __name__=="__main__":
	main()