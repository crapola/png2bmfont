def compute_indices(w,h,cnt):
	ret=[]
	for i in range(cnt):
		a=i//h
		b=i//(w*h)*w*(h-1)
		ret+=[a+w*(i-a*h) + b]
	return ret

def main():
	# test w=4 h=3
	#  0  1  2  3
	#  4  5  6  7
	#  8  9 10 11
	#  ----------
	# 12 13 14 15
	# 16 17 18 19
	# 20 21 22 23
	# -----------
	c=compute_indices(4,3,24)
	test=[0,4,8,1,5,9,2,6,10,3,7,11,12,16,20,13,17,21,14,18,22,15,19,23]
	print(c)
	print(test)
	print([a-b for a,b in zip(c,test)])
	assert c==test

	# test w=5 h=2
	#  0  1  2  3  4
	#  5  6  7  8  9
	# --------------
	# 10 11 12 13 14
	# 15 16 17 18 19
	c=compute_indices(5,2,20)
	test=[0,5,1,6,2,7,3,8,4,9,10,15,11,16,12,17,13,18,14,19]
	print(c)
	print(test)
	print([a-b for a,b in zip(c,test)])
	assert c==test

if __name__=="__main__":
	main()