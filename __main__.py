import struct
import PIL
from PIL import Image

def chunks(lst, n):
	"""Yield successive n-sized chunks from lst. https://stackoverflow.com/a/312464 """
	for i in range(0, len(lst), n):
		yield lst[i:i + n]

def int_from_bit_list(lst):
	# https://stackoverflow.com/a/27165680
	assert len(lst)==8
	return sum(v<<i for i, v in enumerate(reversed(lst)) if v)

def font():
	with open("test.fnt","wb") as file:
		file.write(b"123\0\1\2\xff\x10")
		pass

def compute_indices(w,h,cnt):
	ret=[]
	for i in range(cnt):
		a=i//h
		b=i//(w*h)*w*(h-1)
		ret+=[a+w*(i-a*h) + b]
	return ret

def main():
	img=Image.open("amsdos_128.png")
	#img.show()
	# 1 format is a boolean per pixels (0 and 255's).
	#img=img.convert("1")
	print(f"Image\n.format: {img.format}\n.mode: {img.mode}")
	data=list(img.getdata())
	print(data[:80],"...")
	# Replace 255's with 1's.
	data=[1 if x==255 else 0 for x in data]
	# Convert to bits.
	# First group booleans in pack of 8's.
	data_chunks=list(chunks(data,8))
	print(data_chunks[:10],"...")
	# Convert to ints.
	b=[int_from_bit_list(x) for x in data_chunks]
	print("ints:",b[:80],"...")
	stride=img.width//8
	print(f"Image is {stride} characters wide.")
	# Reorder...
	i=compute_indices(stride,8,len(b))
	z=[b[x] for x in i]
	print(z)



if __name__=="__main__":
	main()